// console.log("Hello World");


// Addition

 function displayNumbers(firstNumber, secondNumber) {
 	
 	let sum = firstNumber + secondNumber;

 	console.log("Displayed sum of " + firstNumber +" and " + secondNumber);
 	console.log(sum);
 };

displayNumbers(5, 15);



// Subtraction

 function provideNumbers(firstNumber, secondNumber) {
 	
 	let difference = firstNumber - secondNumber;

 	console.log("Displayed difference of " + firstNumber +" and " + secondNumber);
 	console.log(difference);
 };

provideNumbers(20, 5);




// Multiplication

function multiplyNum(firstNumber, secondNumber){
	return firstNumber * secondNumber;
};

	let product = multiplyNum(50, 10);
	console.log("The product of 50 and 10: ");
	console.log(product);





//  Quotient

function divideNum(firstNumber, secondNumber){
	return firstNumber / secondNumber;
};

	let quotient = divideNum(50, 10);
	console.log("The product of 50 and 10: ");
	console.log(quotient);




// Circle Radius

function getCircleArea(areaNumber){
	return 3.1416 * (areaNumber ** 2);

};

	let circleArea = getCircleArea(15);
	console.log("The result of getting the area of a circle with 15 radius:");
	console.log(circleArea);




// Average

function getAverage(num1,num2,num3,num4){
	return (num1 + num2 + num3 + num4) / 4;

};

	let averageVar = getAverage(20,40,60,80);
	console.log("The average of 20, 40, 60 and 80: ");
	console.log(averageVar);




// Pass or Fail

function checkIfPassed(scoreNumber, totalNumber){
	return (scoreNumber / totalNumber) * 100;

};
	let isPassed = checkIfPassed(38,50);
	let isPassingScore = isPassed >= 75;
	console.log("Is 38/50 a passing score?");
	console.log(isPassingScore);
