// console.log("Hello World!");

// Functions

	// Parameters and Arguments

		function printInput(){
			let nickname = prompt("Enter your nickname: ");
			console.log("Hi, " + nickname);
		};

		// printInput();

		function printName(name) {
			console.log("My name is " + name);

		}

		printName("Juana");
		printName("John");
		printName("Jane");

		let sampleVariable = "Yui";
		printName(sampleVariable);

		function checkDivisibilityBy8(num){

			let remainder = num % 8;
			console.log("The remainder of " + num + " divisible by 8 is: " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is" + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(64);
		checkDivisibilityBy8(28);


			function checkDivisibilityBy4(num1){

			let remainder1 = num1 % 4;
			console.log("The remainder of " + num1 + " divisible by 4 is: " + remainder1);
			let isDivisibleBy4 = remainder1 === 0;
			console.log("Is" + " divisible by 4?");
			console.log(isDivisibleBy4);
		};

		checkDivisibilityBy4(56);
		checkDivisibilityBy4(95);

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.");
		};

		function invokeFunction(argumentFunction){
			argumentFunction();
		}

		invokeFunction(argumentFunction);

		//  Multiple arguments

		function createFullName(firstName, middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		};

		createFullName("Juan", "Dela");
		createFullName("Jane", "Dela", "Cruz", "Hello");

		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

		function printFullName(middleName, firstName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		};

		printFullName("Juan", "Dela", "Cruz");


		// Return

		function returnFullName(firstName, middleName, lastName){
			// console.log(firstName + " " + middleName + " " + lastName);
			return firstName + " " + middleName + " " + lastName;
			console.log("This message will not be printed.");
		};

		returnFullName("Jeffrey","Smith","Bezos");

		let completeName = returnFullName("Jeffrey","Smith","Bezos");
		console.log(completeName);

		console.log(returnFullName(firstName,middleName,lastName));

		function returnAddress (city, country){
			let fullAddress = city + ", " + country;
			return fullAddress;
		};

		let myAddress = returnAddress("Cebu City","Philippines");
		console.log(myAddress);

		function printPlayerInfo(username,level,job){
				// console.log("Username: " + username);
				// console.log("Level: " + level);
				// console.log("Job: " + job);
				return("Username: " + username + "Level: " + level + "Job: " + job);
		 	};

		 	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
		 	console.log(user1);




		// function listFriends(firstFriend, secondFriend, thirdFriend){
		// console.log("My BFFs are " + firstFriend + ", " + secondFriend + ", " + thirdFriend);
		// };

		// listFriends("Basty", "Church", "Chubi");

		function multiplyNumbers(firstNumber, secondNumber){
			return firstNumber * secondNumber;
		};

		let product = multiplyNumbers(5, 4);
		console.log("The product of 5 and 4: ");
		console.log(product);
